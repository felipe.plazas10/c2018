package controllers.base;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import okhttp3.MediaType;
import play.libs.Json;
import play.mvc.*;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class BaseController extends Controller {

    protected static int AMOUNT_SECONDS_PER_RECORD = Integer.parseInt(System.getenv("MEASUREMENT_LENGTH"));
    protected static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    protected static String DATABASE_URL = "https://heartu-ba7e0.firebaseio.com/";

    protected static final String CONTENT_TYPE = "application/json";

    protected static String ACCESS_TOKEN;

    static{
        try{
            FileInputStream serviceAccount = new FileInputStream("/root/server/firebase-key.json");
            GoogleCredential googleCred = GoogleCredential.fromStream(serviceAccount);

            GoogleCredential scoped = googleCred.createScoped(
                    Arrays.asList(
                            "https://www.googleapis.com/auth/firebase.database",
                            "https://www.googleapis.com/auth/userinfo.email"
                    )
            );

            scoped.refreshToken();
            ACCESS_TOKEN = scoped.getAccessToken();
            System.out.println(ACCESS_TOKEN);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    protected static <T> T bodyAs(Class<T> clazz) {
        Http.RequestBody body = request().body();
        if (body == null || body.asJson() == null) {
            System.out.println("ERROR, this should never happen");
        }
        return Json.fromJson(body.asJson(), clazz);
    }

    protected Result ok(Object object) {
        return object == null ? ok() : Results.ok(Json.prettyPrint(Json.toJson(object))).as(CONTENT_TYPE);
    }

    public static Result ok(String message) {
        return ok( Json.parse("{\"result\":\""+message+"\"}") ).as(CONTENT_TYPE);
    }

    public static Result ok(String property, String text) {
        return ok( Json.parse("{\""+property+"\":\""+text+"\"}") ).as(CONTENT_TYPE);
    }

    protected Result error(String message) {
        return internalServerError(Json.parse("{\"error\":\""+message+"\"}"));
    }

    public static class Session extends Action.Simple {
        @Override
        public CompletionStage<Result> call(Http.Context ctx) {
            String user = ctx.current().session().get("connected");
            if (user == null)
                return CompletableFuture.completedFuture(
                        unauthorized()
                );
            return delegate.call(ctx);
        }
    }

}
