//package controllers.handlers;
//
//import com.amazonaws.services.dynamodbv2.datamodeling.*;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import net.rubyeye.xmemcached.MemcachedClient;
//import net.rubyeye.xmemcached.MemcachedClientBuilder;
//import net.rubyeye.xmemcached.XMemcachedClientBuilder;
//import net.rubyeye.xmemcached.auth.AuthInfo;
//import net.rubyeye.xmemcached.command.BinaryCommandFactory;
//import net.rubyeye.xmemcached.utils.AddrUtil;
//import java.net.InetSocketAddress;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//public class CacheDBHandler extends DBHandler{
//
//    private static CacheDBHandler instance = new CacheDBHandler();
//    private static MemcachedClient client;
//    private final static String MAP_KEY = "keysToUpdate";
//    private static final int PAGINATION = 10;
//    private static final int KEY_EXP_TIME = 60*10;
//
//    static{
//        try {
//            List<InetSocketAddress> servers =
//                    AddrUtil.getAddresses(System.getenv("MEMCACHIER_SERVERS").replace(",", " "));
//
//            AuthInfo authInfo = AuthInfo.plain(System.getenv("MEMCACHIER_USERNAME"), System.getenv("MEMCACHIER_PASSWORD"));
//
//            MemcachedClientBuilder builder = new XMemcachedClientBuilder(servers);
//
//            for (InetSocketAddress server : servers) {
//                builder.addAuthInfo(server, authInfo);
//            }
//
//            builder.setCommandFactory(new BinaryCommandFactory());
//            builder.setConnectTimeout(1000);
//
//            client = builder.build();
//            client.set("foo", 0, "HELLO DUDE");
//            String val = client.get("foo");
//            System.out.println(val);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private CacheDBHandler(){
//    }
//
//    public void save(Object object) throws Exception {
//        super.save(object);
//        String className = object.getClass().getName();
//        setClassesToUpdate(className);
//    }
//
//    public <T>void save(Object object, Class<T>clazz, String... keyValuePairs) throws Exception {
//        super.save(object);
//        String searchKey = generateSearchKey(clazz.getName(), keyValuePairs);
//        setClassesToUpdate(searchKey);
//    }
//
//    public void delete(Object obj) throws Exception {
//        super.delete(obj);
//        String className = obj.getClass().getName();
//        setClassesToUpdate(className);
//    }
//
//    public <T>void delete(Object obj, Class<T>clazz, String... keyValuePairs) throws Exception {
//        String searchKey = generateSearchKey(clazz.getName(), keyValuePairs);
//        setClassesToUpdate(searchKey);
//        super.delete(obj);
//    }
//
//    public <T> Map<String, Object> queryList(String attribute, String value, int pageNum, Class<T> clazz) throws Exception{
//        String searchKey = generateSearchKey(clazz.getName(), attribute, value);
//        QueryListInterface qli = (a, b) -> super.queryList(b[0], b[1], a);
//        return getAndPaginate(clazz, searchKey, qli, pageNum, attribute, value);
//    }
//
//    public <T> Map<String, Object> queryList(Class<T> clazz, Integer pageNum, String... keyValuePairs) throws Exception {
//        String searchKey = generateSearchKey(clazz.getName(), keyValuePairs);
//        QueryListInterface qli = (a, b) -> super.queryList(a, b);
//        return getAndPaginate(clazz, searchKey, qli, pageNum, keyValuePairs);
//    }
//
//    public static CacheDBHandler getInstance(){
//        return instance;
//    }
//
//    private void setClassesToUpdate(String className) throws Exception {
//        String json = client.get(MAP_KEY);
//        Map<String, Integer> map;
//        if (json != null) {
//            map = deserialize(json, Map.class);
//        } else {
//            map = new HashMap<>();
//        }
//        map.put(className, 1);
//        client.set(MAP_KEY, KEY_EXP_TIME, serialize(map));
//    }
//
//    private boolean shouldUpdateAndMark(String searchKey) throws Exception {
//        String json = client.get(MAP_KEY);
//        Map<String, Integer> map;
//        boolean isContained = false;
//        if (json != null) {
//            map = deserialize(json, Map.class);
//            isContained = map.containsKey(searchKey);
//            if (isContained) {
//                map.remove(searchKey);
//                client.set(MAP_KEY, KEY_EXP_TIME, serialize(map));
//            }
//        }
//        return isContained;
//    }
//
//    private <T> Map<String, Object> getAndPaginate(Class<T> clazz, String searchKey, QueryListInterface qli, Integer pageNum,
//                                         String... keyValue) throws Exception{
//        String queryResponse = shouldUpdateAndMark(searchKey) ? null : client.get(searchKey);
//        List<T> results;
//        if (queryResponse == null) {
//            //Get all from dynamoDB
//            QueryResultPage<T> qrp = qli.queryList(clazz, keyValue);
//            results = qrp.getResults();
//            client.set(searchKey, KEY_EXP_TIME, serialize(qrp.getResults()));
//        } else {
//            results = deserialize(queryResponse, List.class);
//        }
//        return paginateResults(results, pageNum);
//    }
//
//    private String generateSearchKey(String className, String... keyValuePairs){
//        String key = className;
//        for (int i = 0; i < keyValuePairs.length; i++) {
//            key += "-" + keyValuePairs[i];
//        }
//        return key;
//    }
//
//    private <T> Map<String, Object> paginateResults(List<T> results, int pageNum){
//        int pageStart = PAGINATION*pageNum - PAGINATION;
//        int pageEnd = pageStart+PAGINATION > results.size() ? results.size() : pageStart+PAGINATION;
//        int pages = -1;
//        //Not proud of this cycle
//        for (int i = 0; i < Integer.MAX_VALUE; i++)
//            if (i*PAGINATION >= results.size() && results.size() < (i*PAGINATION + PAGINATION) ){
//                pages = i;
//                break;
//            }
//        final int numPages = pages > 0 ? pages : 0;
//        return new HashMap<String, Object>(){{
//            put("pages", numPages);
//            put("results", results.subList(pageStart, pageEnd));
//        }};
//    }
//
//    private static String serialize(Object object) throws Exception{
//        String json = new ObjectMapper().writeValueAsString(object);
//        return json;
//    }
//
//    private static <T> T deserialize(String json, Class<T> clazz) throws Exception{
//        T object = new ObjectMapper().readValue(json, clazz);
//        return object;
//    }
//
//    public interface QueryListInterface {
//        QueryResultPage queryList(Class clazz, String... keyValuePairs);
//    }
//}
